> Thank you for contributing to the Open WIN Community by providing feedback via this GitLab issue.
> Please fill in the details under each of the headings below to give your feedback. You may delete this text when you are done.

Note: For support requests, please contact cassandra.gouldvanpraag@psych.ox.ac.uk.

## I'm submitting a ...
> Put an x in the appropriate square brackets

- [ ] bug/error report
- [ ] feature request


## What is the current behaviour?
> Please describe the problem you have encountered or the feature/content you would like to extend. Please include links to the affected page(s).





## What is the expected/preferred behaviour?
> Please describe how the problem could be fixed or what additional feature/content could be included.
> If you are able to, you are welcome to submit new content directly via a merge request. This will ensure you are attributed for your suggestion. See this guide on submitting a merge request via GitLab: https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/3-2-collaborating-fork-their-repo/



## What is the motivation / use case for changing the behaviour?
> Why should we make this fix or include this content?



## Other information
> Please give any additional detail which may be relevant or useful, for example detailed explanation, stacktraces, related issues, links for us to have context.
