---
layout: default
title: 6.1 Mirroring to GitHub
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 17
---


# Mirroring to GitHub
{: .fs-8 }

How to mirror your GitLab repository to GitHub
{: .fs-6 .fw-300 }

---

## Why mirror a repository on GitHub

Both GitHub and GitLab can be used to host your git repositories, whether private or public. Both services offer a slightly differen range of tools and functionalities. Depending on you workflow and personal preferences, one may be better suited or just more convenient to use than the other. GitLab is the default platform at WIN and it therefore makes sense to host your work-related repositories there. However, GitHub has a larger community reach. Making your code / project / software tool also accessible via GitHub can substantially increase engagement from the community (through user uptake, feedback, merge requests, etc.).


## How to mirror your GitLab repository to GitHub

1. Create a GitHub repository that will function as the mirror of your GitLab repo.
2. In your online GitLab account, navigate to the repository you want to mirror.
3. Go to *Settings > Repository*. (Note that you need to have Owner or Maintainer rights to access the settings.)
4. Expand *Mirroring repositories*.
5. Enter the full URL of the GitHub repo that will be the mirror, i.e. ``https://<github username>@github.com/path/to/your/repo.git``.
6. Set the mirroring direction to *Push*.
7. Select an authentication method (default: Password) and enter your GitHub password.
8. Click on *Mirror repository*.

Mirrors are updated automatically. Whenever you push a change to your GitLab repo, your GitHub mirror will be synchronised to reflect the changes.


## Additional resources

For additional information, see the following:

- The official [GitLab documentation for mirroring a repository](https://docs.gitlab.com/ee/user/project/repository/mirror/).
- A [brief guide](https://dev.to/brunorobert/github-and-gitlab-sync-44mn) including also steps for mirroring from GitHub to GitLab.
