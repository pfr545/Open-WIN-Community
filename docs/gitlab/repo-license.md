---
layout: default
title: How to license your materials
parent: Git and GitLab
has_children: false
nav_order: 3
---


# How to license your materials
{: .fs-9 }

A guide to how to license your code and things you should consider
{: .fs-6 .fw-300 }

---

**Contents**
- [Why license your material](#why-license-your-material)
- [What license to choose](#what-license-to-choose)
- [How to apply the license and copyright notice](#how-to-apply-the-license-and-copyright-notice)


## Why license your material

It is essential to license your research outputs so other users understand how you would like them to be re-used, and under what terms.

Important considerations around licensing include the potential for commercial use of your materials, attribution, and integration with any existing licenses if you are releasing material which builds on an existing source (for example, a plug in to FSL).

### Intellectual properties and commercial reuse

In most cases the material which you create while a member of the University are the Intellectual Property of the University of Oxford. In practice this means that [copyright on your work should be attributed to University of Oxford](https://innovation.ox.ac.uk/university-members/commercialising-technology/ip-patents-licenses/software-copyright/) *unless* you are a student working by yourself with the assistnace of no other staff. Please check the [University Statute XVI: Property, Contracts, and Trusts to check whether you can retain personal copyright](https://governance.admin.ox.ac.uk/legislation/statute-xvi-property-contracts-and-trusts#collapse1383596).

If your work has commercial potential, you should explore opportunities for issuing an academic only license and creating a system for commercial entities to pay for the use of your work. This is in line with [Oxford University Innovation (OUI) recommendations](https://innovation.ox.ac.uk/university-members/commercialising-technology/). OUI are pleased to explore the commercial potential of your work with you and will set up services for commercial licensing.

### Attribution

You deserve to be credited for your work. Although there is an established norm to cite academic papers for the avoidance of plagiarism, this norm is not well established for academic code. Any license which you issue is a legally binding agreement; adding an attribution clause will go a long way to normalising the expectation that you should receive credit for the work you do.


### Existing licenses

If your code builds on existing material, the originators of that material may have issued it with a licence which requires you to share new material with the same level of freedom. This is know as a "share alike" licence. Please carefully check your source material for existing license and ensure you comply with any terms for re-use or distribution.

## What license to choose

We recommend the following licenses for diffferent types of work. These have been created with specific products in mind, hence licenses written for software might not be well suited to other creative works.
- **Software**: [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0). This license requires attribution (be sure to include a 'Attribution' section in your README, or event better a CITATION.cff), and copyright notice to the University of Oxford. This licence does not restrict reuse to commercial or non-comercial applications.
- **Data or other creative works** (for example documentation): [CC-BY-4.0 license](https://creativecommons.org/licenses/by/4.0/). This enables others to reuse your work as long as they attribute you as the originator. This license also allows the University to retain copyright.  This licence does not restrict reuse to commercial or non-comercial applications.

The University also has detailed [recommendations on open source licensing](https://researchsupport.admin.ox.ac.uk/reporting/openaccess#collapse393821). Please review these recommendations if you are unsure which license is appropriate.  

For a more complete exploration of software licensing, we recommend you visit the [The Turing Way handbook to reproducible, ethical and collaborative data science](https://the-turing-way.netlify.app/reproducible-research/licensing/licensing-software.html)

## How to apply the license and copyright notice

To apply a licence, you simply have to state the name of the licence somewhere in your material. This could be in the `README` file, or in a separate `LICENSE` file. You may wish to copy the full terms of the license text into the file, but this is not necessary. If you include a license file, it is good practice to include it in the [root directory](https://en.wikipedia.org/wiki/Root_directory) of your project, to ensure it is easily found.

For the Copyright notice, you should include the below in our `README` and as a footnote on any published pages (replace `YYYY` with the year the material was created):
> Copyright, YYYY, University of Oxford. All rights reserved
