---
layout: default
title: 5.1 GitLab issues
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 15
---


# GitLab Issues
{: .fs-8 }

Use GitLab issues invite feedback and track improvements
{: .fs-6 .fw-300 }

---

Coming soon
{: .label .label-yellow }
