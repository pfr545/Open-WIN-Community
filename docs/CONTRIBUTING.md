---
layout: default
title: Contributing
parent: Home
has_children: false
nav_order: 3
---

# Contributing
{: .fs-9 }

How to contribute to this repository
{: .fs-6 .fw-300 }

---

This repository is designed to be used by members of the Wellcome Centre for Integrative Neuroimaging (WIN), and we expect most contributions will come from WIN members. **If you are not a member of WIN, we still welcome your feedback and input!** Please contact cassandra.gouldvanpraag@psych.ox.ac.uk to discuss how you can be involved.

---

We want to ensure that every user and contributor feels welcome and included in the WIN Open Neuroimaging community. We ask all contributors and community members to follow the [code of conduct](../community/CODE_OF_CONDUCT) in all community interactions.

**We hope that this guideline document will make it as easy as possible for you to get involved.**

Please follow these guidelines to make sure your contributions can be easily integrated in the projects. As you start contributing to the WIN Open Neuroimaging, don't forget that your ideas are more important than perfect [pull requests](https://opensource.stackexchange.com/questions/352/what-exactly-is-a-pull-request).

## 1. Contact us
In the first instance, we encourage you [connect with the Open WIN Community](../contact). Let us know that you are reading the repository and you would like to contribute! You might like to introduce yourself on the #welcome channel on our [Slack workspace](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/contact/#open-win-slack-)! 👋

## 2. Check what we're working on

<!-- You can see the things we are working on in the [Open WIN Repository Trello board](https://trello.com/b/u4FqvNJv). Have a look through and see where we have known gaps. You are also welcome to suggest any of your own improvements. -->

We also invite you to review our [roadmap](../community/roadmap) and see how we are progressing against our community milestones.

## 3. Join us at a documentation hack!
We will be holding regular "documentation hacks", where you will be guided through the process of contributing to the repository, and can participate in a git tutorial if you'd benefit from this. Take a look at our [events page](../events) to find out when the next documentation hack will be held.

At the documentation hack, you will hear from members of the [Open Neuroimaging Project Working Groups](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/community-who/#win-open-neuroimaging-working-group) and learn about the progress of each tool. You will learn how to use each tool, be invited to contribute feedback to the tool developers, and invited to write "how to" guides to walk novice users through the process of shearing their research outputs using the [Open WIN Tools](../tools).

We will also use the documentation hacks to plan as a community, learn about what policy or guidance we need to support working open at WIN, and think about how we would like to develop. This will include community-led planning of the [Open WIN Ambassadors scheme](../ambassadors).

## Or, 4. Jump straight in!

If you are confident using git, markdown, and GitLab pages, you are very welcome to [submit an issue to our GitLab repository](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues), and submit a merge request.

### New to GitLab?
If you'd like to try out GitLab at your own pace, take a look at the [training materials available here](../gitlab).
