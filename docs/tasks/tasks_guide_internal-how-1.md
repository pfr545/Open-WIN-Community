---
layout: default
title: Add a new task
parent: Open Tasks
has_children: false
nav_order: 2
---


# Adding to the tasks repository
{: .fs-9 }


---

<!-- ![open-tasks](../../../img/img-open-tasks-flow.png) -->


## Contents
- [How do I access the WIN Open Tasks repository?](#how-do-i-access-the-win-open-tasks-repository)
- [What software package should I use?](#what-software-package-should-i-use)
- [Where can I get advice on running experiments online?](#where-can-i-get-advice-on-running-experiments-online)
- [Sharing PsychoPy/Pavlovia tasks](#sharing-psychopypavlovia-tasks)
- [How do I add my task to the Open WIN Tasks repository(s)?](#how-do-i-add-my-task-to-the-open-win-tasks-repositorys)
- [Who can I get further advice?](#who-can-i-get-further-advice)


## How do I access the WIN Open Tasks repository?
Access the Open Tasks repository via the [WIN Open Science GitLab group](https://git.fmrib.ox.ac.uk/open-science). To contribute a task to this repository, you will need to be added as a member - please contact [Laurence Hunt](https://www.win.ox.ac.uk/people/laurence-hunt) or [Cassandra Gould van Praag](https://www.win.ox.ac.uk/people/cassandra-gould-van-praag) to request permission.

## What software package should I use?
If you're starting out with developing a new task, we recommend using [PsychoPy](https://www.psychopy.org/). There are several advantages to PsychoPy over other potential platforms. It contains both a *builder* mode (for people want to use a GUI to build their experiment) and a *coder* mode (for people who want to be as flexible as possible) - and the builder automatically creates usable Python code that you can edit in coder mode, which is a great way to learn. It has good, reliable *timing*, that has been tested across platforms. It can run experiments *online* or in the lab, and has its own server ([Pavlovia](https://pavlovia.org)) where you can easily collect behavioural data online. And it has excellent documentation and a large and growing userbase.

Of course, there are several other options out there for designing your task, including [Psychtoolbox](http://psychtoolbox.org/), [Presentation](https://www.neurobs.com/), [Gorilla](https://gorilla.sc/), and [Unity](https://unity.com). If you develop your task using one of these platforms, you are still very welcome to share your task via the WIN Open Tasks repository!

## Where can I get advice on running experiments online?
We recommend looking at this [Introduction to Online Experiments](https://online-ws.readthedocs.io/en/latest/), developed by the Brain and Cognition lab. These are strongly tailored towards people who are based at the University of Oxford and trying to develop their tasks, and includes guidance on programming your study, hosting it, recruiting participants, collecting data and obtaining ethical approval.

## Sharing PsychoPy/Pavlovia tasks
We think the [Pavlovia](https://pavlovia.org) system for task sharing is a great infrastructure for maintaining up-to-date versions of your tasks, and sharing them with the rest of the world. As you may be aware, the back-end of the Pavlovia system is managed by GitLab, which can be accessed [here](https://gitlab.pavlovia.org). At WIN, we have a group called [OPEN_WIN](https://gitlab.pavlovia.org/open_win) where we are gathering together projects that people are working on within Pavlovia. To contribute a task to this repository, you will need to be added as a member - please contact [Laurence Hunt](https://www.win.ox.ac.uk/people/laurence-hunt) or [Dejan Draschkow](https://www.psych.ox.ac.uk/team/dejan-draschkow) to request permission.

## How do I add my task to the Open WIN Tasks repository(s)?
Once you've been added as a member of the WIN Open Tasks reposistory and/or the OPEN_WIN Pavlovia repository (see above), you can add your repositories to these groups! In WIN GitLab, go to your repository, and then click on "Members", then "Invite Group", and choose "open-science" in your group to invite. In PsychoPy, go to [Pavlovia GitLab](https://gitlab.pavlovia.org/), and then click on "Settings", "Members", "Invite Group", and choose "OPEN_WIN" as the group. We recommend that you add the repository to the group with 'reporter' permissions. See below for an example of how to do this:

![Pavlovia example](../../../img/pavlovia_OPENWIN_walkthrough.png)


## Who can I get further advice?
In first instance, feel free to email [Laurence Hunt](https://www.win.ox.ac.uk/people/laurence-hunt) for any further information you need.
