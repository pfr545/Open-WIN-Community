---
layout: default
title: For external researchers
parent: Open Tasks
has_children: false
nav_order: 3
---






# User Guide for External Researchers
{: .fs-9 }

A guide for external researchers on how to use the Open WIN tasks repository
{: .fs-6 .fw-300 }

---

## For external researchers
External users are able to search the repository for experimental tasks which individual research teams have chosen to make openly available. Searches may be conducted using keyword terms based on the author, theoretical application, or stimulus type. Task materials may be deposited to support publications as supplementary methods material, or they may form the main body of research in new task development.

## Access the task repository
Go to [https://git.fmrib.ox.ac.uk/open-science/tasks](https://git.fmrib.ox.ac.uk/open-science/tasks)
