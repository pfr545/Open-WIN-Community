---
layout: default
title: Track engagement
parent: Open MR Protocols
has_children: false
nav_order: 5
---


# Tracking engagement
{: .fs-9 }


---

<!-- ![open-protocols](../../../img/img-open-mrprot-flow.png) -->


You can track how many times your protocol has been viewed and downloaded from the `Engagement Statistics` panel. Views and downloads are separated into internal to Oxford (where the user has logged in with an SSO) or external. When reporting on the impact of your entry, remember to also include the zenodo download and view statistics, if you used zenodo to create a doi.
