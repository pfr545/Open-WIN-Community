---
layout: default
title: Data management plans
parent: Open Data
has_children: true
nav_order: 6
---



# Data management plans
{: .fs-9 }

Materials for helping you write an effective data management plan
{: .fs-6 .fw-300 }

---


Most grant funding bodies will require you to submit a data management plan (DMP) with your application. These plans should detail the types of data you will collect and how you will ensure it is stored appropriately. In many cases you will also be asked to describe your plans for sharing your data and other outputs.

It is also good practice to create a data management plan for smaller projects in a larger funded program. Creating these plans is a useful exercise to help you think about our project outputs in a structured way early on, enabling you to handle outputs effectively when they are generated.

## University resources and policy

There are a number of central University resources available to support you in developing your data management plan. Please cross check against these policies and contacts to ensure the information you submit is up to date.

### Resources

- [Guidance on Research Data Management](https://researchdata.ox.ac.uk), including links to training courses to support the writing of data management plans.
- [DMP Online](https://dmponline.dcc.ac.uk), an online tool for building a data management plans. Sign in using the "Institutional credentials" button then search for "University of Oxford". You will be redirected to your SSO login. You will have access to a series of templates built to fit the requirements of each listed funder, with advice and boxes to provide the required information.

### Policy

- [Policy on the Management of Research data and Records](https://www.dcc.ac.uk/sites/default/files/documents/events/workshops/RDM-services/Oxford-RDM-policy.pdf)
- [Data protection policy (Compliance)](https://compliance.admin.ox.ac.uk/data-protection-policy)
- [Information Security Policy and Implementation Guidance (InfoSec)](https://www.infosec.ox.ac.uk/guidance-policy)
- [Oxford Open Access](https://openaccess.ox.ac.uk)
- [Licensing and IP guidance via Oxford University Innovation (OUI)](https://innovation.ox.ac.uk/university-members/commercialising-technology/ip-patents-licenses/). See also the short [Open WIN Community guide to licensing code](../../gitlab/repo-license)
- [Research contracts support](https://researchsupport.admin.ox.ac.uk/contracts)

## WIN specific examples

Below we have collected some example responses in the formats required by [Wellcome](#wellcome-data-management-plan---discovery-platform-2022), [BBSRC](#bbsrc-data-management-plan---7t-upgrade-2021) and [MRC](#mrc-data-management-plan---mica-2018). Further examples are very welcome! If you have some material you'd like to share, please take a look at our [contributing guide](../../CONTRIBUTING).

**The details in the plans below were correct at the time of writing, however some aspects of how WIN manages your data may have changed. Please refer to the following WIN policies to ensure you have the most up to dat information about our data handling:**

- WIN data retention policy - <mark>coming soon</mark>
- WIN handling of personal Information - <mark>coming soon</mark>
- WIN data storage and back up infrastructure - <mark>coming soon</mark>
- [Data security](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/Shared%20Documents/Data_Security.pdf)


<!-- <details>
 <summary><b>TITLE</b></summary><br>

 BODY TEXT

 <br><br>
</details> -->
### Wellcome Data Management Plan - Career Development Award (Dec 2022)
This is the data management plan for a fellowship application. It discusses sharing of a new sequence (called DIPPI), sharing of the developed tools, and sharing of the acquired data.

Prepared by Michiel Cottaar with feedback from Cassandra Gould van Praag and <mark>others</mark>.

<details>
   <summary><b>Expand for application sections</b></summary><br>
   I will make any research outputs for access and reuse following FAIR principles and the open science guidelines and tools from WIN (https://www.win.ox.ac.uk/open-win). This is in line with my previous software releases such as file-tree (https://pypi.org/project/file-tree/), pipe-tree (https://open.win.ox.ac.uk/pages/ndcn0236/pipe-tree/), and MCMRSimulator.jl (https://open.win.ox.ac.uk/pages/ndcn0236/mcmrsimulator.jl/dev/).
   <br><br>
   <details>
      <summary><b>DIPPI Sequence</b></summary><br>
   Sequence development requires access to Siemens' proprietary sequence development environment, which is governed by Oxford's Master Research Agreement with Siemens Healthineers. According to this agreement, any sequence transfer requires contracts between those sites, the University of Oxford, and Siemens. The WIN physics group has extensive experience negotiating such contracts in a timely manner and in sharing their MRI sequence innovations around the world. I will exploit this expertise (collaborators Prof. Miller and Dr. Wu) to share the DIPPI sequence with all parties who can meet the contractual requirements of the Master Research Agreement.<br><br>
   MRI protocols, which describe suggested acquisition schemes for DIPPI, will be made available under a CC-BY 4.0 license through the WIN MR Protocol database (https://open.win.ox.ac.uk/protocols).
   <br><br>
   </details>
   <details>
      <summary><b>Data</b></summary><br>
   MRI data is uniquely identifiable at most stages of processing and should be treated as personal data under GDPR. It is therefore not appropriate to share this data freely. WIN is developing a data portal which can provide gated access for bonafide researchers to GDPR sensitive data, and unrestricted access to anonymous data. Data shared on the portal will be deidentified as far as possible (for example by removing facial features and identifying metadata) while still retaining the research value of individual participant structure. Anonymous data and animal data will be shared on the portal under a CC-BY-4.0 license.GDPR sensitive data will be share with under terms of appropriate data usage agreement which will protect the privacy of participants and acknowledgement of the contribution. All participants will be asked for consent to share data in the way proposed, as described in the relevant ethical approvals.
   <br><br>
   </details>

   <details>
      <summary><b>Software</b></summary><br>
   Software tools will be developed to fit the DIPPI model to estimate myelin thickness from DIPPI data. These tools will be included in a pipeline that can be used to fully process from raw DIPPI data to myelin maps. Once these tools are robust, they will be released as a python package on the WIN's own Gitlab page (https://git.fmrib.ox.ac.uk/) under a permissive open source license (MIT or Apache 2.0) and registered with both pypi and anaconda for easy installation. To improve discoverability, these tools would also be listed as affiliated with the FMRIB's Software Library (FSL; https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/OtherSoftware), which is used by over a thousand labs around the world. This software will be given its own DOI using Zenodo. <br><br>
   The MR simulator (MCMRSimulator.jl) will be developed in public following modern software engineering practices including regular releases, a change log, automated testing, and documentation for both users and developers (https://open.win.ox.ac.uk/pages/ndcn0236/mcmrsimulator.jl/dev/). Version 0.3 is already released under the Apache 2 permissive license. This will facilitate early discovery of any bugs not caught by the automated tests as well as maximising the chance of it being used by someone to discover new MRI probes of microstructure.
   <br><br>
   </details>
</details>

### Wellcome Data Management Plan - Discovery Platform (2022)

This DMP was submitted to support an application to Wellcome to convert WIN into a [Discovery Platform](https://wellcome.org/grant-funding/schemes/discovery-awards).

Prepared by Stuart Clare, Cassandra Gould van Praag and <mark>others!</mark>

<details>
 <summary><b>Expand for application sections</b></summary><br>


    <details>
     <summary><b>What outputs will your research generate?</b></summary><br>

     The platform will generate: MRI scans (human and pre-clinical); electrophysiological recordings; histological images; behavioural measures; software code for the analysis of such data.

     <br><br>
    </details>

    <details>
     <summary><b>What metadata and documentation (e.g., the methodology of data collection and way of organising data) will accompany the outputs?</b></summary><br>

     Researchers are encouraged to ensure that MRI data be stored in non-proprietary nifti format using the Committee on Best Practices in Data Analysis and Sharing (COBIDAS)-recommended Brain Imaging Data Structure (<a href="https://github.com/bids-standard">BIDS</a>) naming conventions. The centre is also actively engaged in development of new community standards for specific applications e.g. BIDS <a href="https://github.com/bids-standard/bep001">BEP001</a>), <a href="https://docs.google.com/document/d/15tnn5F10KpgHypaQJNNGiNKsni9035GtDqJzWqkkP6c/edit">BEP005</a>, <a href="https://docs.google.com/document/d/1xeiFACAnK_X64kL6XoXo8vpZ9Bn6B5Xd3j59Le1clRE/edit#heading=h.4k1noo90gelw">BEP025</a>, <a href="https://bids-specification--881.org.readthedocs.build/en/881/04-modality-specific-files/10-microscopy.html">BEP031</a>). Researchers will also be encouraged to apply and contribute to community standards for all non-MRI data, such as <a href="https://www.nwb.org/">Neurodata Without Boarders</a> for electrophysiology. Researchers will be encouraged to ensure that code for project-specific analysis is well commented and that larger packages are released with appropriately accessible documentation. We also have links with the originators of the <a href="https://www.go-fair.org/fair-principles/">FAIR principles</a> (<a href="https://oerc.ox.ac.uk">Oxford e-Research Centre</a>) and will support researchers to ensure that all outputs will be released with appropriate persistent identifiers and licensing to enable effective reuse. Researchers will also be supported in using our <a href="https://open.win.ox.ac.uk/protocols/">Open Protocols Database</a> which captures full acquisition methodology in a sharable and version controlled repository.

     <br><br>
    </details>

    <details>
     <summary><b>When will these outputs be made available?</b></summary><br>

     We aim to share our data with as few restrictions as possible, and in a timely manner. Timeframes will be influenced by the nature of the data.

     <br><br>
    </details>

    <details>
     <summary><b>Where will you make these outputs available?</b></summary><br>

     We have infrastructure to fulfil the unique requirements of our discipline and legislature (notably GDPR). Human imaging data will be made available for sharing via our <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/">Open Data Repository</a> and non-human animal data will be shared via the <a href="https://open.win.ox.ac.uk/DigitalBrainBank/#/">Digital Brain Bank</a>. In all cases the PIs will be able to determine the appropriate level of external data access to control release in line with funder guidelines. Datasets are accompanied by quality descriptors. The <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/">WIN Open Protocols repository</a> will be used to store and communicate image acquisition metadata. These protocols can be made available for sharing with external users via a web interface and DOI to ensure developer attribution. Other outputs generated by the platform may include research software or analysis routines. The Centre encourages free open-source languages, distributed with appropriate documentation to enable effective reuse (e.g., the <a href="https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/">FMRIB Software Library</a>, which has been free for non-commercial use since its inception). Additional research software is provided through our self-hosted <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/">GitLab</a> instance.

     <br><br>
    </details>

    <details>
     <summary><b>How will they be discovered and accessed by the research community? (e.g., via presentations/press releases)</b></summary><br>

     Data and protocol outputs will be given a DOI, to allow them to be referenced in publications and presentations. Where the DOI is issued by <a href="https://zenodo.org">Zenodo</a>, it will be indexed and tracked by <a href="https://www.openaire.eu">OpenAIRE</a>. Where DOIs are issued by the University, outputs will be indexed in the institutional repository (<a href="https://ora.ox.ac.uk">Oxford Research Archive</a>).

     <br><br>
    </details>

    <details>
     <summary><b>Are there possible restrictions to data sharing or embargo reasons?</b></summary><br>

     MRI data is uniquely identifiable at most stages of processing and should be treated as personal data under GDPR. This necessitates restrictions around sharing to ensure appropriate reuse. Our infrastructure is being designed to support the level of access and frequency of release as prescribed by individual research projects (e.g., with embargos if required or near-instantaneously when possible) and a review of risks relating to identifiability of the data.

     <br><br>
    </details>

    <details>
     <summary><b>How will data and metadata be stored, backed up and preserved?</b></summary><br>

     Raw MRI data are stored on a <mark>check-summed RAID system with two-disk redundancy</mark>. A copy is automatically archived onto tape format for long-term data storage. This enables data retention for a minimum of ten years, with an additional copy held in an off-site location.  Where outputs are issued with a DOI, that issuer assume responsibility for long term preservation and archive.

     <br><br>
    </details>

    <details>
     <summary><b>What resources (e.g., financial and time) will be dedicated to outputs management and ensuring all data is findable, accessible, interoperable, and reproducible?</b></summary><br>

     A platform-dedicated database manager, together with the WIN IT support team, will support the hardware and software for output management.  We have developed <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/">open science community pages</a> alongside other University resources to provide ongoing guidance (e.g. <a href="https://researchdata.ox.ac.uk/home/sharing-your-data/">Research Data Services</a> and <a href="https://ox.ukrn.org">Reproducible Research Oxford</a>.

     <br><br>
    </details>

    <details>
     <summary><b>What IP will your research generate?</b></summary><br>

     Tools for transforming results at one scale to another.  Data acquisition methods (e.g., MRI sequences, M/EEG or histological methods).

     <br><br>
    </details>

    <details>
     <summary><b>How will you protect this IP?</b></summary><br>

     Software tools will be incorporated into FSL, which is available for free for non-commercial use since its inception but licenced via <a href="https://innovation.ox.ac.uk">Oxford University Innovation</a> for commercial uses. Data acquisition IP will be protected in ways specified in our agreements with the equipment manufacturers (e.g., Siemens).

     <br><br>
    </details>

    <details>
     <summary><b>How will the IP be used to achieve health benefits?</b></summary><br>

     Where appropriate, software tools or data acquisition methods will be shared with other sites via the clinical FSL platform, or via equipment manufacturers.

     <br><br>
    </details>

    <details>
     <summary><b>Provide the name and contact details for the person in your organisation (e.g. Technology Transfer Officer or Business Development executive) who can act as a point of contact for Wellcome in connection with the protection and commercialisation of this IP.</b></summary><br>

     Gavin Brown, Research Contracts Lead; gavin.brown@admin.ox.ac.uk

     <br><br>
    </details>

<br><br>
</details>

<!-- <details>
 <summary><b>TITLE</b></summary><br>

 BODY TEXT

 <br><br>
</details> -->


### BBSRC Data management plan - 7T upgrade (2021)

This plan was prepared to support an application to the BBSRC to fund a hardware upgrade to the WIN 7T system. The facility supports a wide range of research projects, combining data from various sources and, as such, a bespoke data management plan will be required for each individual project. The document below outlines the expectations for data management and sharing that the Centre applies to all projects run on the facility.

Prepared by Peter Jezzard, Stuart Clare, Iske Marshall and Cassandra Gould van Praag

<details>
 <summary><b>Expand for application sections</b></summary><br>

    <details>
     <summary><b>Support for individual investigators to manage their data</b></summary><br>

     The Centre offers flowcharts and supporting documents to guide investigators using imaging data in their studies through the data management principles. Specifically, this includes:
        - Guidance on completing Data Protection Screening, Data Protection Assessments and, where necessary, Data Protection Impact Assessments (DPIA) (<a href="https://www.medsci.ox.ac.uk/divisional-services/support-services-1/data-privacy/privacy-by-design">Divisional "Privacy by Design" guide</a>).
        - Steps needed to ensure that data can be considered as anonymous before sharing (<a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/can-i/#deidentification">deidentification guide</a>).
        - Steps needed, such as and contract writing, to enable data to be shared on closed or open data repositories (<a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/can-i/">governance guide</a>).
     The Centre’s <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/community-who/#community-coordinator---cassandra-gould-van-praag-sheher">Open Science Community Engagement Coordinator</a> supports researchers by giving advice on data management plans. The University’s <a href="https://www.infosec.ox.ac.uk">Information Security Team</a> and <a href="https://compliance.admin.ox.ac.uk">Information Compliance Team</a> provide specific advice on data security and GDPR.

    <br><br>
    </details>

    <details>
     <summary><b>Data areas and data types</b></summary><br>

     When acquiring MRI data, the participant’s name and date of birth are recorded, but stored separately from any imaging data, linked by means of a key. Imaging data may be combined with other measures such as behavioural, physiological, questionnaire (symptomatic and lifestyle) or genetic data. Such cases must be considered on a project-by-project basis.

     Raw MRI data are stored on a <mark>check-summed RAID system with two-disk redundancy. A copy is automatically archived onto tape format for long-term data storage. This enables data retention for a minimum of ten years, with an additional copy held in an off-site location. A subset of the scan metadata (project number, unique scan i.d., subject details, date of scan and protocol names) are held in a centrally managed database for link purposes [check this is still correct].</mark>


     <br><br>
    </details>

    <details>
     <summary><b>Standards and metadata</b></summary><br>

    Researchers are encouraged to ensure that MRI data be stored in non-proprietary nifti format using the Committee on Best Practices in Data Analysis and Sharing (COBIDAS)-recommended Brain Imaging Data Structure (<a href="https://github.com/bids-standard">BIDS</a>) naming conventions. The centre is also actively engaged in development of new community standards for specific applications e.g. BIDS <a href="https://github.com/bids-standard/bep001">BEP001</a>), <a href="https://docs.google.com/document/d/15tnn5F10KpgHypaQJNNGiNKsni9035GtDqJzWqkkP6c/edit">BEP005</a>, <a href="https://docs.google.com/document/d/1xeiFACAnK_X64kL6XoXo8vpZ9Bn6B5Xd3j59Le1clRE/edit#heading=h.4k1noo90gelw">BEP025</a>, <a href="https://bids-specification--881.org.readthedocs.build/en/881/04-modality-specific-files/10-microscopy.html">BEP031</a>).

     The <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/">WIN Open MR Protocols repository</a> will be used to store and communicate image acquisition metadata. Each deposited protocol is version controlled to ensure transparency and reproducibility and contains all information necessary to reproduce the acquisition on an appropriately licensed scanner. These protocols can be made available for sharing with external users via a web interface and DOI to ensure developer attribution.

     <br><br>
    </details>

    <details>
     <summary><b>Methods for data sharing</b></summary><br>

     We have infrastructure to fulfil the unique requirements of our discipline and legislature (notably GDPR). Human imaging data will be made available for sharing via our <a href="https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/how/#win-open-data-portal">Open Data Repository</a> and non-human animal data will be shared via the <a href="https://open.win.ox.ac.uk/DigitalBrainBank/#/">Digital Brain Bank</a>. In all cases the PIs will be able to determine the appropriate level of external data access to control release in line with BBSRC guidelines. Datasets are accompanied by quality descriptors.

     Other outputs generated through receipt of this award may include research software or analysis routines. The Centre encourages free open-source languages, distributed with appropriate documentation to enable effective reuse (e.g., the FMRIB Software Library, <a href="https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/">FSL</a>, which has been free for non-commercial use since its inception). Additional research software is provided through our self-hosted GitLab instance.


     <br><br>
    </details>

    <details>
     <summary><b>Timeframes</b></summary><br>

     We aim to share our data with as few restrictions as possible, and in a timely manner. Timeframes will be influenced by the nature of the data. Our infrastructure is being designed to support the level of access and frequency of release as prescribed by individual research teams (e.g., with embargos if required or near-instantaneously when possible).

     <br><br>
    </details>

<br><br>
</details>

### MRC Data management plan - MICA (2018)

Submitted in support of an application for a single study pharmacological behavioural and imaging project.

Prepared by Catherine Harmer, Susanna Murphy ([Psychopharmacology and Emotion Research Laboratory](https://www.psych.ox.ac.uk/research/psychopharmacology-and-emotion-research-laboratory)) and Cassandra Gould van Praag

<details>
 <summary><b>Expand for application sections</b></summary><br>


    <details>
     <summary><b>Type of study</b></summary><br>

     Experimental medicine study in healthy volunteers and in patients with treatment resistant depression

     <br><br>
    </details>


    <details>
     <summary><b>Types of data</b></summary><br>

     <ol>
        <li>Participant name, date of birth and other demographic data (e.g. gender and handedness); address, contact</li> information and General Practitioner’s contact information
        <li> Psychological Questionnaire scores including validated measures of depression</li>
        <li> Performance data (reaction time and accuracy) from computerised neuropsychological tests</li>
        <li> Physiological data (pupil diameter) collected during neuropsychological testing</li>
        <li> Structural and functional neuroimaging data i.e. MRI brain scans</li>
    </ol>

     <br><br>
    </details>


    <details>
     <summary><b>Format and scale of the data</b></summary><br>

     Personal data such as name, date of birth and contact information will be collected for the purposes of recruitment and recording informed consent, before being anonymised for use on future paperwork and in electronic databases.

     Anonymised demographic data and questionnaire scores will be collected on a Case Report Form (CFR), and then entered into an electronic database. Electronic records of demographic and questionnaire scores will be stored in a universally acceptable comma separated values (CSV) format to maximise transferability between software products.

     Performance data, physiological recordings and MRI scans will be collected on a computerised system and converted from proprietary into open formats where possible and appropriate for the measure. Raw MRI data will be received in the universal ‘dicom’ format and converted into ‘nifti’ images.

     Analysis of performance, physiological and MRI data will be conducted in using open source software where possible e.g. using the FMRIB Software Library (FSL) MRI analysis tools. Statistical analysis will be conducted using R, SPSS and MATLAB using a controlled dataset from the database after database lock. Calculated endpoint data will be recorded in CSV format and transferred to a database.

     <br><br>
    </details>


    <details>
     <summary><b>Methodologies for data collection / generation</b></summary><br>

     Personal and demographic information will be collected on a CRF designed explicitly for use in this study once the study protocol has been finalised. The CRF may be on paper or electronic, as deemed appropriate for the protocol. An electronic CRF (eCRF) may be preferable to enable greater transferability and validation at the point of capture.

    Computerised performance data will be collected using standardised behavioural paradigms which have been validated by reproduction and replication by this group and others Physiological data will be collected using a EyeLink 1000 system integrated with the behavioural stimulus control. MRI data will be acquired on a 7 Tesla Siemens  system and stored in nifit format using the Committee on Best Practices in Data Analysis and Sharing (COBIDAS) recommended <a href="https://bids.neuroimaging.io">Brain Imaging Data Structure (BIDS)</a> naming conventions .


     <br><br>
    </details>


    <details>
     <summary><b>Data quality and standards</b></summary><br>

     Data collection will be carried out in accordance with Good Laboratory and clinical Practice (GLP/GCP) procedures, the gold standard requirement for an experimental medicine study. All staff involved will have been trained to GLP level and/or Good Clinical Practice (GCP) levels.  Study monitoring and data management will be conducted in accordance with establishedSOPs (e.g. SOP for the preparation and maintenance of the Trial Master File).  The Trial Master File will be regularly audited by internal staff.

    CRF will be monitored throughout the study to ensure data quality in accordance with the study monitoring plan. An eCRF may include validation at the point of data entry. All physiological monitoring equipment (e.g. for the collection of pupilometry) and MRI systems undergo regular calibration and quality assessment procedures as scheduled by the Wellcome Centre for Integrative Neuroimaging (WIN). Additional quality assessments will be conducted prior to data analysis. All performance measures will be obtained with repeat sampling to ensure accuracy in calculated (averaged) scores. All individuals engaged in data collection will be required to undergo training and/or demonstrate competence in running the data acquisition systems.

    Data validation will be conducted on all clinical data in accordance with the study Data management and validation plan following standard SOPs prior to Database lock, unblinding and Statistical Analysis.

    Standardised computer systems used have been validated and qualified in accordance with regulatory requirements for computerised systems used in clinical investigations, including a full audit trail capability.

     <br><br>
    </details>


    <details>
     <summary><b>Managing, storing and curating data </b></summary><br>

     Paper Case Report Forms will be stored in a secure location at the Investigator site.  Any paperwork detailing sensitive personal data, as well as the Trial Masterfile, will be stored in a locked cabinet in a restricted access office. Once a participant completes the study, the CRF will be monitored, and once signed and one copy sent for entry into the database.

     All data stored within a computerised system is anonymised, each participant will be allocated a unique number identifier.  All data stored within a computerised system is stored securely and backed up on a secure server in accordance with the University of Oxford Policy on the Management of Research Data and Records.

     Specific consideration is given to the management and storage of MRI data, due to the large size of individual data files, the sensitive nature of the information they contain and the expense involved in their collection. The WIN has implemented the following systems and policies to ensure this valuable data is managed appropriately: Raw MRI data is stored on a computer memory system where multiple copies of each data file exist on independent disks (check-summed RAID system with two-disk redundancy). This system of duplication reduces the likelihood that all data are lost should there be a hardware (disk) failure. A copy of all raw data is automatically archived onto tape format, for cost-effective and durable long-term data storage. The tape archive is periodically cloned and taken off site. The tape archive enables WIN to retain a copy of raw MRI data for a minimum of ten years, with an additional copy held in a different (off-site) location to minimise the risk of physical damage (e.g. fire) damaging the no-site archive. A subset of the scan metadata (project number, unique scan id, subject details, date of scan and protocol names) are held in a centrally managed database that connects this data with the archived scans. This metadata archive is subject to daily backup to an on-site tape system. These processes minimize the likelihood that this valuable data is lost in its raw format and can be recovered for a minimum of ten years.

     During the MRI analysis process, intermediate results are stored on a check-summed RAID device with single disk redundancy. Tape based archives of intermediate results may be taken at any time. Final results may be archived to tape, with clones taken off-site. The process of generating the intermediate and final results from the raw data will be managed using version controlled custom programming maintained in an online repository (e.g. GitLab.com), to apply analysis algorithms developed as part of open source software resources such as FSL. A transferable digital copy of the computer analysis environment and software packages employed (i.e. a ‘Docker’ image; https://www.docker.com) will ensure that it is possible to recreate any stage/version of the intermediate processing in the exact computing environment it was originally generated. These measures will ensure that the analyzed data and findings may be reproduced from the raw data at any point. A record of the archived data and the analysis decisions made will be held in the electronic lab notebooks (eLNs) which will be used to log interactions with the data throughout the analysis process.  

     <br><br>
    </details>


    <details>
     <summary><b>Metadata standards and data documentation</b></summary><br>

     All methods used to collect the data, including the MRI protocol, are documented in the protocol and the case report form.  The coding and analysis of data is specified in advance using the Data Management Plan and the Statistical Analysis Plan. These documents form part of the archived materials for the study allowing a full audit trail, and will exist alongside the version controlled software repository for the study and digital copy of the computer analysis environment.

     <br><br>
    </details>


    <details>
     <summary><b>Data preservation strategy and standards</b></summary><br>

     Alongside the tape archive of raw MRI data held by the WIN for a minimum of ten years, as described above, all further anonymised data is held for ten years either on a secure university server using the <a href="https://ora.ox.ac.uk">Oxford Research Archive (ORA)</a> and/or as paper copies (i.e. of questionnaires and case report forms) archived according to University policy (<a href="https://researchsupport.admin.ox.ac.uk/files/universitycoresop5archivingofessentialdocumentspdf">University of Oxford Core SOP 005-Archiving of Essential Documents</a>) allowing retrieval within 48 hours and destroyed after a 10 year period.  Documentation of archives on eLNs will allow a full audit trail to be completed.

     <br><br>
    </details>


    <details>
     <summary><b>Formal information/data security standards</b></summary><br>

     We will comply with the <a href="https://www1.admin.ox.ac.uk/councilsec/compliance/gdpr/">General Data Protection Regulation (GDPR)</a> established in May 2018. Following the introduction of the new GDPR, the university has updated its policy and management of <a href="https://www.infosec.ox.ac.uk/guidance-policy">information security</a>. This includes a review of all data types, confidentiality levels and their storage detailed in an Information Asset Register (IAR).  The Psychiatry Department holds its own IAR overseen by a departmental Information Governance Officer.  Information and data from this study will be reviewed in accordance with this IAR to ensure appropriate protection. The WIN also aims to reach the standards of <a href="https://www.iso.org/isoiec-27001-information-security.html">ISO 27001 compliance</a>.

     <br><br>
    </details>


    <details>
     <summary><b>Main risks to data security</b></summary><br>

     The main risk to data security would be a failure of the anonymization process. The study will be run to GCP and the study documentation and process will be set up to reduce this risk (by anonymising and allocating a unique identifier for each participant). This will be formally monitored as part of the data management responsibilities of the study monitor.  The anonymised study data will be initially stored on secure, password protected, University servers which are accessible only by the study researchers.

     <br><br>
    </details>


    <details>
     <summary><b>Suitability for sharing</b></summary><br>

     All anonymised, non-personal data are suitable for sharing. These data will contain no identifiable information and may be made accessible to other researchers on request or under the terms of an appropriate licence, e.g. the Creative Commons CC0 or CC-BY, after an exclusivity period given in below.

     <br><br>
    </details>


    <details>
     <summary><b>Discovery by potential users of the research data</b></summary><br>

     The research report, including metadata where appropriate, will be published in an open access peer-reviewed journal allowing other researchers to find and access the report of the study outcomes. The report will also be published as a preprint (e.g. on <a href="https://www.biorxiv.org">bioRxiv</a>) and the investigation listed as a registered report on a <a href="https://www.clinicaltrials.gov">clinical trial database</a> will allow easy identification. The summary data will be available on the study website, with links to the full anonymised results database hosted and maintained on an appropriate online platform (e.g. <a href="https://openneuro.org">OpenNeuro</a> or other as designated by the journal publishers). Data and metadata may alternatively be shared via the University of Oxford Wellcome Centre for Integrative Neuroimaging (WIN) repository, which is currently under construction and will enable the generation of permanent digital object identifiers (DOIs). Data and metadata will be shared in line with a policy published on the study webpage along with appropriate licence information.

     <br><br>
    </details>


    <details>
     <summary><b>Governance of access</b></summary><br>

     The investigators are the custodians of the data (asset owners) and will make decisions regarding the access to the data.  These decisions will be in line with <a href="http://www.mrc.ac.uk/research/research-policy-ethics/data-sharing/data-sharing-population-and-patient-studies/">MRC policy on data sharing</a>.

     <br><br>
    </details>


    <details>
     <summary><b>The study team’s exclusive use of the data</b></summary><br>

     The data from this study will be made available after the results have been published in a peer reviewed journal at which time the summary of results will also appear on the study website. If the data have not been published within 24 months of study end, then they will be made available to interested researchers via the pathways described above. Data will be shared openly via an online repository after a further 12 months (total of 36 after study end).

     <br><br>
    </details>


    <details>
     <summary><b>Restrictions or delays to sharing, with planned actions to limit such restrictions </b></summary><br>

     Only fully anonymised data will be shared. Identifiable facial features may be obscured from MRI data as required by the hosting platform.

     <br><br>
    </details>


    <details>
     <summary><b>Regulation of responsibilities of users </b></summary><br>

     External users will be bound by MRC policy on data sharing and the applied licensing agreements.  A data-sharing agreement may be issued and signed by appropriate authorities before data are released or analyses are performed on behalf of the requester, or be implicit in the terms of use of an online repository.

     Data-sharing agreements will prohibit any attempt to (a) identify study participants from the released data or otherwise breach confidentiality, (b) make unapproved contact with study participants.

     <br><br>
    </details>

    <details>
     <summary><b>Responsibilities</b></summary><br>

     The PI has overall responsibility for ensuring these requirements are met.  However, this role will be supported by the other investigators and by the researcher co-ordinating this trial.  S/he will ensure data is secure and provide quality assurance of the data in collaboration with regulatory and information technology expertise available within the University

     <br><br>
    </details>

    <details>
     <summary><b>Relevant institutional, departmental or study policies on data sharing and data security</b></summary><br>

     Data Management Policy & Procedures
     <li> Overall <a href="http://www.admin.ox.ac.uk/rdm/dmp/">guidance from University of Oxford on data management plans</a>
     <li> University of Oxford <a href="http://researchdata.ox.ac.uk/university-of-oxford-policy-on-the-management-of-research-data-and-records/">Policy on the Management of Research data and Records</a>
    <br><br>

     Data Security Policy
     <li> Updated policy following launch of GDPR in May 2018: <a href="https://www.infosec.ox.ac.uk/guidance-policy">University Information and Security Policy</a></li>
    <br><br>

     Data Sharing Policy
     See above. We will also follow the MRC policies:
     <li> <a href="https://www.ukri.org/publications/mrc-data-sharing-policy/">MRC policy on research data sharing</a></li>
     <li> <a href="mrc.ukri.org/publications/browse/mrc-policyand-guidance-on-sharing-of-research-data-from-population-and-patient-studies/">MRC policy on sharing of research data from population and patient studies</a></li>
    <br><br>

     Institutional Information Policy
     <li> <a href="https://www.infosec.ox.ac.uk/guidance-policy">Information Security Policy</a></li>
    <br><br>

     Other
     <li> <a href="http://openaccess.ox.ac.uk/wp-content/uploads/sites/2/2018/03/University-of-Oxford-OA-Publications-Policy-01-03-2018.pdf">University of Oxford open access policy</a></li>
     <li> <a href="mrc.ukri.org/research/policies-and-guidance-forresearchers/open-access-policy/">MRC open access policy</a></li>

     <br><br>
<!-- </details> -->
<br><br>
<!-- </details> -->
